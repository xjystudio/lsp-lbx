# LSP-LBX的个人主页
本文所有可使用链接地址如下：
- https://lsp-lbx.tk
- https://www.lsp-lbx.tk
- https://lsp-lbx.vercel.app
- https://lsp-lbx-xiejiayu.vercel.app
## 内部链接直达
- [HTML Fork Bomb(bug未修复)](https://lsp-lbx.tk/flies/forkbomb.html)
- [Intel 4004电路图](https://lsp-lbx.tk/flies/intel4004.gif)
- [32位的Windows Fork Bomb](https://lsp-lbx.tk/flies/wfb-x86.exe)
- [爱因斯坦论文译版《论动体的电动力学》](https://lsp-lbx.tk/flies/article.pdf)
- [danmaku!一个标准街机难度的弹幕游戏!](https://lsp-lbx.tk/flies/danmaku.html) [hard mode(120fps)](https://lsp-lbx.tk/flies/danmaku-hard.html)
- [私人搭建的Discuz论坛!](http://4a60051q73.zicp.vip/discuz)
#### 注:本网页为LBX-LSP的github pages镜像站，可提高访问速度，加强网页质量，原地址见:[LBX's personal web](http://4a60051q73.zicp.vip/)
